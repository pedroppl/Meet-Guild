package com.project.pablo.meetguild;



import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Chat_Recibidor extends Service {

    static Context ctx;
    private static Activity activity;

    static  CountDownLatch latch = new CountDownLatch(1); //Paramos el hilo hasta que otro hilo/s le avisen.

    static String mensaje;

    static Socket skSRV;

    static boolean estaEnBackground = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {


        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        System.out.println("INICIANDO SERVICIO RECIBIDOR");

        skSRV = Conectador.skcli;

        ctx = getApplicationContext();


        new HiloServidor().start();

        return super.onStartCommand(intent, flags, startId);
    }

    static class HiloServidor extends Thread {


        Socket elSocket;
         String mensajeCifrado;


        public HiloServidor() {
            super();
            this.elSocket = skSRV;
        }


        public void sendNotification(String texto) {

            //Get an instance of NotificationManager//



               // PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                    //    new Intent(ctx, Guild_interface.class), 0);

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(ctx)
                                .setSmallIcon(R.drawable.ic_menu_share)
                                .setContentTitle(ctx.getResources().getString(R.string.notificacion_texto))
                                .setContentText(texto)
                               // .setContentIntent(contentIntent)
                                .setAutoCancel(true)
                                .setDefaults(-1);


                // Gets an instance of the NotificationManager service//

                NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);


                mNotificationManager.notify(001, mBuilder.build());




        }

        private String desencriptar(String criptograma, SecretKey clave) throws IOException {
            try {
                Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
                c.init(Cipher.DECRYPT_MODE, clave);
                byte[] decVal = new Base64().decode(criptograma.getBytes("UTF-8"));

                byte[] bmensaje = c.doFinal(decVal);
                return new String(bmensaje, "UTF-8");
            } catch (Exception e) {
                mensaje = null;
                e.printStackTrace();
                elSocket.close();
                //System.out.println("Error al desencriptar");
                return null;
            }
        }

        @Override
        public void run() {

            try {

                while (elSocket != null) {
                    System.out.println("Esperando mensaje");

                    InputStreamReader lector = new InputStreamReader(elSocket.getInputStream(), "UTF-8");

                    BufferedReader br = new BufferedReader(lector);

                    mensajeCifrado = br.readLine();

                    byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");

                    MessageDigest sha;

                        sha = MessageDigest.getInstance("SHA-1");

                        miclaveBits = sha.digest(miclaveBits);

                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

                        mensaje = desencriptar(mensajeCifrado, claveTrans);

                        try {

                             Guild_interface.cola.put("-recibido:-" + mensaje+ "-1993End09Of14Stream-");

                        }catch(Exception e){

                            e.printStackTrace();

                        }

                        if(!estaEnBackground) {
                         //   while ((mensaje) != "") {
                                Guild_interface.escribirEnChat(mensaje);
                                mensaje = "";
                        //    }

                        }else{
                            sendNotification(mensaje);
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();

               Guild_interface.hayErrorConexion = true;


               // Guild_interface.escribirEnChat("Error de conexion");
                mensaje = "";

            }
        }
    }

}
