package com.project.pablo.meetguild;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class CreateGuildActivity extends AppCompatActivity {


    Button mButtonEnviarCreateGuild;

    EditText edNombreGuild;
    EditText edNombreJuego;
    EditText edGuildDescript;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_guild);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mButtonEnviarCreateGuild = (Button) findViewById(R.id.btnEnviarGuildNueva);
        edNombreGuild = (EditText) findViewById(R.id.edNombreGuild);
        edNombreJuego = (EditText) findViewById(R.id.edNombreJuego);
        edGuildDescript = (EditText) findViewById(R.id.edGuildDescript);


        mButtonEnviarCreateGuild.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                class enviaGuildNueva extends Thread{

                    @Override
                    public void run(){

                        try {

                            Boolean guildCreadaExito = false;

                            while(!guildCreadaExito) {
                                String nuevaGuild = "new19930914-" + edNombreGuild.getText().toString() + "-separadorDescript09-14-"+ edGuildDescript.getText().toString() + "-separadorJuego09-14-" + edNombreJuego.getText().toString() + "-1993End09Of14Stream-";

                                //edGuildDescript.getText();


                                PrintWriter pw2 = null;

                                pw2 = new PrintWriter(Conectador.skcli.getOutputStream(), true);
                                pw2.println(nuevaGuild);

                                System.out.println("Esperando Respuesta del servidor");
                                InputStreamReader lectorGetUserNameAndGuild = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");

                                BufferedReader bruserNameAndGuild = new BufferedReader(lectorGetUserNameAndGuild);
                                String respuesta = bruserNameAndGuild.readLine();

                                if (respuesta.equals("registro_ok")) {

                                    guildCreadaExito = true;

                                    System.out.println("Guild creada con exito.");

                                }else if(respuesta.equals("guild_name_exist")){

                                    guildCreadaExito = false;

                                }
                            }

                            InputStreamReader lectorGetUserNameAndGuild = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");

                            BufferedReader bruserNameAndGuild = new BufferedReader(lectorGetUserNameAndGuild);
                            String respuesta = bruserNameAndGuild.readLine();

                            System.out.println("Recibido user y guild, intentando lanzar ventana GUILD" + " --->> " + respuesta);

                            String[] userAndGuild = respuesta.split("-19930914-");

                            String user = userAndGuild[0];
                            String guild = userAndGuild[1];
                            System.out.println(user + "---------- " + guild);

                            Intent i = new Intent(getApplicationContext(), Guild_interface.class);

                            i.putExtra("user", user);
                            i.putExtra("guild", guild);

                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                            getApplicationContext().startActivity(i);


                            CreateGuildActivity.this.finish();



                        } catch (IOException e) {
                            e.printStackTrace();
                            CreateGuildActivity.this.finish();

                        }


                    }


                }


                new enviaGuildNueva().start();

            }
        });



    }

}
