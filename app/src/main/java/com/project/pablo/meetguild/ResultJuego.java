package com.project.pablo.meetguild;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Pablo-PC on 20/10/2016.
 */
public class ResultJuego implements Serializable {
    boolean code;
    int status;
    String message;
    ArrayList<Juego> Juegos;


    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Juego> getJuegos() {
        return Juegos;
    }

    public void setJuegos(ArrayList<Juego> juegos) {
        this.Juegos = juegos;
    }
}


