package com.project.pablo.meetguild;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ContentFrameLayout;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class ChatMainActivity extends AppCompatActivity {





    private static final String TAG = "ChatActivity";

    public static ChatArrayAdapter chatArrayAdapter;
    public static ListView listView;
    public static EditText chatText;
    private Button buttonSend;

    public static String mensaje_recibido = "";
    Intent intent;
    public static boolean side = false;
    static BlockingQueue<String> cola = new ArrayBlockingQueue<>(20);
    static public Socket  skcli;
    static Activity mactivity;

    @Override
    protected void onResume() {
        super.onResume();

        try {

            InputStream instream = new FileInputStream("chat.txt");


            if (instream != null) {
                // prepare the file for reading
                InputStreamReader inputreader = new InputStreamReader(instream);
                BufferedReader buffreader = new BufferedReader(inputreader);

                String line;

                // read every line of the file into the line-variable, on line at the time
                do {
                    line = buffreader.readLine();

                    if(line.contains("-recibido: -")){
                        chatArrayAdapter.add(new ChatMessage(false, line.substring(12)));
                    }else if(line.contains("-enviado: -")){
                        chatArrayAdapter.add(new ChatMessage(true, line.substring(11)));
                    }

                } while (line != null);

            }
        } catch (Exception ex) {
            // print stack trace.

            ex.printStackTrace();
        }




    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity_main);


       // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

       // StrictMode.setThreadPolicy(policy);


        skcli = Conectador.skcli;

        mactivity = this;

        buttonSend = (Button) findViewById(R.id.buttonSend);

        listView = (ListView) findViewById(R.id.listView1);

        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
        listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) findViewById(R.id.chatText);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {


                    if(chatText.getText().equals("") || chatText.getText() == null){


                    }else{
                      //  return   sendChatMessage(chatText.getText().toString()); //or whatever method you want to call thats currently not working
                    }


                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(chatText.getText().toString().equals("") || chatText.getText() == null){


                }else{
                    sendChatMessage(chatText.getText().toString()); //or whatever method you want to call thats currently not working
                }
               // chatText.setText("");
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });



        /*
        try {
            new Chat_Recibidor(this, this, skcli);//Chat_Recibidor
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
    }

    static void escribirEnChat(String texto){

        ChatMainActivity.mensaje_recibido = texto;



        mactivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {


                if (ChatMainActivity.mensaje_recibido != "") {
                    ChatMainActivity.side = false;
                    ChatMainActivity.chatArrayAdapter.add(new ChatMessage(ChatMainActivity.side, ChatMainActivity.mensaje_recibido));
                    ChatMainActivity.mensaje_recibido = null;
                }

            }

        });

    }



    public  boolean sendChatMessage(String msg){

        Boolean lado = true;

        try {

            ChatMainActivity.cola.put("-enviado: -" + msg);
        }catch(Exception e){

            e.printStackTrace();
        }


        chatArrayAdapter.add(new ChatMessage(lado, msg));


        new Chat_Enviador(msg, skcli);

        ChatMainActivity.chatText.setText("");

        return true;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
