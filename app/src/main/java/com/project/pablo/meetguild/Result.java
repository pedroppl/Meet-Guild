package com.project.pablo.meetguild;

import java.io.Serializable;
import java.util.ArrayList;


public class Result  implements Serializable {
    boolean code;
    int status;
    String message;
    ArrayList<Guild> Guilds;


    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Guild> getGuilds() {
        return Guilds;
    }

    public void setGuilds(ArrayList<Guild> guilds) {
        this.Guilds = guilds;
    }
}


