package com.project.pablo.meetguild;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {



    String URL;
    String URLJuegos;
    String URLfiltroNombre;
    Context mcontext = this;
/////////////////////


    Button mButtonGuildCreate;
    Button botonFiltro;
    Button botonActualiza;


    private List<Guild> guilds;
    private List<Juego> juegos;
    private RecyclerView rv;
    String Errormessage = "No se han podido obtener los datos.";
    private Context mcont = this;//Capturamos el contexto de la activity para pasarselo al adapter.



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        botonFiltro = (Button) findViewById(R.id.button_filtro);
        mButtonGuildCreate = (Button) findViewById(R.id.button_guild_create);
        botonActualiza = (Button) findViewById(R.id.button_actualizar_main);

        botonActualiza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                guilds.clear();
                                rv = null;
                                rv = (RecyclerView) findViewById(R.id.rv);
                                LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
                                rv.setLayoutManager(llm);
                                rv.setHasFixedSize(true);

                                showList();


                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                //Toast.makeText(mcontext, "Solicitud NO aceptada", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                };

                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
                builder.setMessage(mcontext.getResources().getString(R.string.actualizar_lista_clanes_pregunta)).setPositiveButton(mcontext.getResources().getString(R.string.solicitud_usuario_respuesta1), dialogClickListener)
                        .setNegativeButton(mcontext.getResources().getString(R.string.solicitud_usuario_respuesta2), dialogClickListener).show();


            }
        });



        mButtonGuildCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CreateGuildActivity.class);

                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                getApplicationContext().startActivity(i);
            }
        });


        botonFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence options[] = new CharSequence[] {"Nombre", "Juego"};

                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
                builder.setTitle("Filtrar por...");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // the user clicked on colors[which]


                        if(which == 0){

                            showListFiltroNombre();
                        }
                        if(which == 1){


                            getListJuegos();

                            //final CharSequence colors2[] = new CharSequence[] {"Juego1", "Juego2", "Juego3", "Juego4"};


                        }

                    }
                });
                builder.show();
            }
        });









        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        rv=(RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        URL = "http://192.168.1.137/meetguild/api/allguilds";
        URLfiltroNombre = "http://192.168.1.137/meetguild/api/orderallguilds";
        URLJuegos = "http://192.168.1.137/meetguild/api/alljuegos";
        initializeData();

    }

    private void initializeData(){

        showList();
    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(guilds, mcont);

        rv.setAdapter(adapter);
    }



    private void getListJuegos(){

        final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());


        client.setMaxRetriesAndTimeout(3, 1500);//Establecemos un numero maximo de retrys y un timeout.

        client.get(URLJuegos, new JsonHttpResponseHandler() {



            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                ResultJuego result = new ResultJuego();
                Gson gson = new Gson();

                result = gson.fromJson(String.valueOf(response), ResultJuego.class);
                //Log.i("response", String.valueOf(response));
                if (result != null) {
                    // if (result.getCode()) {

                    juegos = new ArrayList<Juego>();

                    juegos.addAll(result.Juegos);


                    ArrayList<String> listing = new ArrayList<String>();
                    Juego item;
                    for(int i=0;i<juegos.size();i++)
                    {
                        item = juegos.get(i);
                        listing.add(item.toString());
                        //getPath is a method in the customtype class which will return value in string format

                    }


                    final CharSequence[] csJuegos =  listing.toArray(new CharSequence[listing.size()]);

                    AlertDialog.Builder builder2 = new AlertDialog.Builder(mcontext);
                    builder2.setTitle("Selecciona el juego");
                    builder2.setItems(csJuegos, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // the user clicked on colors[which]


                            showListFiltroJuego(csJuegos[which].toString());

                           // Toast.makeText(MainActivity.this, "has pulsado: " + juegos.get(which).toString(), Toast.LENGTH_SHORT).show();



                        }
                    });
                    builder2.show();
                    Errormessage = "";
                } else{

                    Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showListFiltroJuego(String juego){


        guilds.clear();
        rv = null;
        rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);


        final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());


        client.setMaxRetriesAndTimeout(3, 1500);//Establecemos un numero maximo de retrys y un timeout.

        client.get(URL+"/"+juego, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result = new Result();
                Gson gson = new Gson();

                result = gson.fromJson(String.valueOf(response), Result.class);
                //Log.i("response", String.valueOf(response));
                if (result != null) {
                    // if (result.getCode()) {

                    guilds = new ArrayList<Guild>();

                    guilds.addAll(result.Guilds);

                    initializeAdapter();

                    Errormessage = "";
                } else{

                    Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showListFiltroNombre(){

        guilds.clear();
        rv = null;
        rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);


        final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());


        client.setMaxRetriesAndTimeout(3, 1500);//Establecemos un numero maximo de retrys y un timeout.

        client.get(URLfiltroNombre, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result = new Result();
                Gson gson = new Gson();

                result = gson.fromJson(String.valueOf(response), Result.class);
                //Log.i("response", String.valueOf(response));
                if (result != null) {
                    // if (result.getCode()) {

                    guilds = new ArrayList<Guild>();

                    guilds.addAll(result.Guilds);

                    initializeAdapter();

                    Errormessage = "";
                } else{

                    Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void showList() {
        final ProgressDialog progreso = new ProgressDialog(this);
        //AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        AsyncHttpClient client = new AsyncHttpClient();
        // using a socket factory that allows self-signed SSL certificates.
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());


        client.setMaxRetriesAndTimeout(3, 25000);//Establecemos un numero maximo de retrys y un timeout.

        client.get(URL, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result = new Result();
                Gson gson = new Gson();

                result = gson.fromJson(String.valueOf(response), Result.class);
                //Log.i("response", String.valueOf(response));
                if (result != null) {
                    // if (result.getCode()) {

                    guilds = new ArrayList<Guild>();

                    guilds.addAll(result.Guilds);

                    initializeAdapter();

                    Errormessage = "";
                } else{

                    Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();

                Toast.makeText(getApplicationContext(), Errormessage, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

                drawer.openDrawer(GravityCompat.START);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            Intent i = new Intent(mcontext, ListaSolicitudesUsuario.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            mcontext.startActivity(i);



            // Handle the camera action
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
