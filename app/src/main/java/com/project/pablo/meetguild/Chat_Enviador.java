package com.project.pablo.meetguild;



import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Chat_Enviador {

    static String  mensaje = null;
    Socket skCLI;
    public Chat_Enviador(String msg, Socket msocket){

        mensaje = msg;

        skCLI = msocket;

        Thread enviador = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                        byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");
                        //Le aplicamos a la clave la firma HASH en SHA-1
                        MessageDigest sha = MessageDigest.getInstance("SHA-1"); //Usamos SHA-1 por que siempre es multiplo de 16
                        //lo cual es obligatorio en clave transparente

                        miclaveBits = sha.digest(miclaveBits);
                        //Cogemos los 128 bits primeros de la clave en HASH.
                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        //Este implementa una interfaz de clave transparente.
                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES/ECB/PKCS5Padding");

                        String mensajeencriptado = null;

                        mensajeencriptado = encriptarAES(mensaje, claveTrans);

                        PrintWriter pw = new PrintWriter(skCLI.getOutputStream(), true);
                        pw.println(mensajeencriptado);

                } catch (Exception e) {
                    System.err.println("Error de conexion al socket - enviador");

                    Guild_interface.hayErrorConexion = true;

                    //Toast.makeText(Guild_interface.mactivity.getApplicationContext(), "Error en la conexion", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }
            }
        });
        enviador.start();
    }
    public String encriptarAES(String mensaje, SecretKey clave){

        Cipher c = null;
        byte[] criptograma = null;
        try {
            c = Cipher.getInstance("AES/ECB/PKCS5Padding");

            c.init(Cipher.ENCRYPT_MODE, clave);
            byte[] encVal = c.doFinal(mensaje.getBytes("UTF-8"));

            //Debemos hacer un PADDING a Base64bits para que funcione correctamente.
            //Para esto, nos descargamos un archivo llamado:  commons-codec 1.10.jar
            criptograma = new Base64().encode(encVal);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new String(criptograma, Charset.defaultCharset());

    }



}
