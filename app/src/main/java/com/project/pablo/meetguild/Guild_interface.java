package com.project.pablo.meetguild;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import android.support.v7.app.ActionBarDrawerToggle;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Guild_interface extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    TextView nombre;

    String userName;
    String guildName;


    private static final String TAG = "ChatActivity";

    public static ChatArrayAdapter chatArrayAdapter;
    public static ListView listView;
    public static EditText chatText;
    private Button buttonSend;

    public static String mensaje_recibido = "";
    Intent intent;
    public static boolean side = false;
    static BlockingQueue<String> cola = new ArrayBlockingQueue<>(20);
    static public Socket skcli;
    static Activity mactivity;

    static Boolean hayErrorConexion = false;


    static Context mictx;



    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guild_interface);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if(!isMyServiceRunning(Chat_Recibidor.class)){
            startService(new Intent(getBaseContext(), Chat_Recibidor.class));
        }else{
            stopService(new Intent(Guild_interface.this, Chat_Recibidor.class));
            startService(new Intent(getBaseContext(), Chat_Recibidor.class));
        }

        if(!isMyServiceRunning(HiloEscritorArchivoChat.class)) {
            startService(new Intent(getBaseContext(), HiloEscritorArchivoChat.class));
        }else{
            stopService((new Intent(Guild_interface.this, HiloEscritorArchivoChat.class)));
            startService(new Intent(getBaseContext(), HiloEscritorArchivoChat.class));
        }

        if(!isMyServiceRunning(EnviadorPing.class)) {
            startService(new Intent(getBaseContext(), EnviadorPing.class));
        }else{
            stopService((new Intent(Guild_interface.this, EnviadorPing.class)));
            startService(new Intent(getBaseContext(), EnviadorPing.class));
        }



        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger_menu);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nombre = (TextView) findViewById(R.id.textView2);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {

             userName = extras.getString("user");
             guildName = extras.getString("guild");

            // and get whatever type user account id is
            nombre.setText(getResources().getString(R.string.guild_interface_saludo1) + userName + getResources().getString(R.string.guild_interface_saludo2) + "\n"+ guildName);

        }else{
            nombre.setText("");
        }


        skcli = Conectador.skcli;

        mictx = Guild_interface.this;


        mactivity = this;

        buttonSend = (Button) findViewById(R.id.buttonSend);

        listView = (ListView) findViewById(R.id.listView1);

        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
        //listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) findViewById(R.id.chatText);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {


                    if(chatText.getText().equals("") || chatText.getText() == null){

                    }else{
                        //  return   sendChatMessage(chatText.getText().toString()); //or whatever method you want to call thats currently not working
                    }
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(chatText.getText().toString().equals("") || chatText.getText() == null){


                }else{

                    if(hayErrorConexion){
                        Toast.makeText(mictx, "La conexion se ha perdido", Toast.LENGTH_SHORT).show();
                    }

                    sendChatMessage(chatText.getText().toString());
                }

            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.err.println("activity destruida");


    }

    @Override
    protected void onPause() {
        super.onPause();

        Chat_Recibidor.estaEnBackground = true;

    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();


        if(hayErrorConexion){
            Toast.makeText(mactivity, "La conexion se ha perdido", Toast.LENGTH_SHORT).show();


            if(isMyServiceRunning(EnviadorPing.class)) {
                stopService((new Intent(Guild_interface.this, EnviadorPing.class)));
            }
            if(isMyServiceRunning(HiloEscritorArchivoChat.class)) {
                stopService((new Intent(Guild_interface.this, HiloEscritorArchivoChat.class)));
            }
            if(isMyServiceRunning(Chat_Recibidor.class)) {
                stopService((new Intent(Guild_interface.this, Chat_Recibidor.class)));
            }

            hayErrorConexion =false;



            Intent i = new Intent(getApplicationContext(), g_sign_SignInActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            getApplicationContext().startActivity(i);

                finish();

        }



        System.out.println("Entrando en onResume");

        Chat_Recibidor.estaEnBackground = false;

        try {

                chatArrayAdapter = null;
                chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
                chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    listView.setSelection(chatArrayAdapter.getCount() - 1);
                     }
                });
                listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

                listView.setAdapter(null);
                listView.setAdapter(chatArrayAdapter);

                BufferedReader input = null;
                File file = null;

                file = new File(getFilesDir(), "chat.txt");

                input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String linea;

            String cadenacompleta = "";
            while((linea = input.readLine()) != null){

                if (linea.endsWith("-1993End09Of14Stream-")) {
                    linea = linea.substring(0, linea.length() - 21);
                    System.out.println(linea);
                    cadenacompleta += linea;

                    if (cadenacompleta.contains("-recibido:-")) {
                        chatArrayAdapter.add(new ChatMessage(false, cadenacompleta.substring(11, cadenacompleta.length())));
                    } else if (cadenacompleta.contains("-enviado:-")) {
                        chatArrayAdapter.add(new ChatMessage(true, cadenacompleta.substring(10, cadenacompleta.length())));
                    }

                    cadenacompleta = "";


                   // break;
                }else{

                    System.out.println(linea);
                    cadenacompleta += linea+"\r\n";

                }

            }


            System.out.println("SALIENDO DEL WHILE");

        /*
            if((line = input.readLine()) == null){

            }else {
                do {
                    if (line.contains("-recibido: -")) {
                        chatArrayAdapter.add(new ChatMessage(false, line.substring(12)));
                    } else if (line.contains("-enviado: -")) {
                        chatArrayAdapter.add(new ChatMessage(true, line.substring(11)));
                    }
                    line = input.readLine();
                } while (line != null);
                //  }
            }
            */


        } catch (Exception ex) {
            // print stack trace.

            ex.printStackTrace();
        }
    }


    public  boolean sendChatMessage(String msg){

        Boolean lado = true;

        try {

            Guild_interface.cola.put("-enviado:-" +  msg + "-1993End09Of14Stream-");
        }catch(Exception e){

            e.printStackTrace();
        }

        chatArrayAdapter.add(new ChatMessage(lado, msg));

        new Chat_Enviador(userName +": " + msg, skcli);

        Guild_interface.chatText.setText("");

        return true;

    }



    static void escribirEnChat(String texto){




        Guild_interface.mensaje_recibido = texto;


        if(Guild_interface.mensaje_recibido == null){
            mactivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {


                        Guild_interface.side = false;
                        Guild_interface.chatArrayAdapter.add(new ChatMessage(Guild_interface.side, "Server: connection lost"));
                        Guild_interface.mensaje_recibido = "";
                }
            });
        }else {
            mactivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (Guild_interface.mensaje_recibido.equals("") || Guild_interface.mensaje_recibido != null) {
                        Guild_interface.side = false;
                        Guild_interface.chatArrayAdapter.add(new ChatMessage(Guild_interface.side, Guild_interface.mensaje_recibido));
                        Guild_interface.mensaje_recibido = "";
                    }
                }
            });
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //return super.onKeyDown(keyCode, event);

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent backtoHome = new Intent(Intent.ACTION_MAIN);
        backtoHome.addCategory(Intent.CATEGORY_HOME);
        backtoHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(backtoHome);


        moveTaskToBack(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.guild_interface, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.guild_menu_calendario) {

            Intent i = new Intent(getApplicationContext(), EventosListaActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            i.putExtra("usuario", userName);
            getApplicationContext().startActivity(i);

        } else if (id == R.id.guild_menu_storyboard) {

            Intent i = new Intent(getApplicationContext(), MessageBoardListActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            i.putExtra("usuario", userName);

            getApplicationContext().startActivity(i);




        } else if (id == R.id.guild_menu_lista_solicitudes) {

            Intent i = new Intent(getApplicationContext(), ListaSolicitudesLider.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            i.putExtra("usuario", userName);

            getApplicationContext().startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;



    }

}
