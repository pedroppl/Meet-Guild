package com.project.pablo.meetguild;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;

//Revisar nombres de variables.

public class RVAdapterListaEventos extends RecyclerView.Adapter<RVAdapterListaEventos.ListaEventosViewHolder> {


    static Context mcontext;
    static String username;

    public static class ListaEventosViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView fecha;
        TextView texto;//Probablemente esto sea la descripcion
        TextView titulo;
        String estado;

        ListaEventosViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvusuario);
            fecha = (TextView)itemView.findViewById(R.id.txt_fecha_evento);
            texto = (TextView)itemView.findViewById(R.id.txt_texto_evento);
            titulo = (TextView) itemView.findViewById(R.id.txt_titulo_evento);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Toast.makeText(mcontext, "No implementado", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(mcontext, EventoSingleEventActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                    i.putExtra("fecha", fecha.getText().toString());
                    i.putExtra("titulo", titulo.getText().toString());
                    i.putExtra("texto", texto.getText().toString());
                    i.putExtra("username", username);
                    mcontext.startActivity(i);

                }
            });
        }



    }

    List<Evento> ListaEventos;

    RVAdapterListaEventos(List<Evento> ListaEventos, Context ctx, String nombreuser){
        this.ListaEventos =  ListaEventos;

        username = nombreuser;
        mcontext = ctx;//Recuperamos el contexto y lo almacenamos.
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ListaEventosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_event, viewGroup, false);
        ListaEventosViewHolder svh = new ListaEventosViewHolder(v);

        return svh;
    }

    @Override
    public void onBindViewHolder(ListaEventosViewHolder eventoViewHolder, int i) {
        eventoViewHolder.fecha.setText(ListaEventos.get(i).fecha);
        eventoViewHolder.texto.setText(ListaEventos.get(i).descripcion);
        eventoViewHolder.titulo.setText(ListaEventos.get(i).titulo);

    }

    @Override
    public int getItemCount()  {


        try {
            return ListaEventos.size();
        }catch(Exception e){

            return 0;
        }
    }



}
