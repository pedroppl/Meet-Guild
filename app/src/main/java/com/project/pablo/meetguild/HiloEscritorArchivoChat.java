package com.project.pablo.meetguild;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.concurrent.BlockingQueue;

import static android.content.Context.MODE_PRIVATE;



public class HiloEscritorArchivoChat extends Service{

    InputStream is;
    OutputStream os;
    private BlockingQueue<String> cola;
    Context ctx;
    FileOutputStream outputStream = null;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        System.out.println("INICIANDO SERVICIO ESCRITOR");

        ctx = this;
        cola = Guild_interface.cola;

        /*
        try {

            BufferedReader input = null;
            File file = null;
            file = new File(getFilesDir(), "chat.txt");
            input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

            if(input.readLine() == null){

                outputStream = openFileOutput("chat.txt", Context.MODE_APPEND);
                outputStream.write("-enviado: - Bienvenido al chat.".getBytes());
                outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
}*/

        Thread hiloescribe = new Thread( new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        String mensaje;
                        mensaje = cola.take();
                        outputStream = openFileOutput("chat.txt", Context.MODE_APPEND);
                        outputStream.write((mensaje + "\r\n").getBytes());

                        outputStream.close();

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        hiloescribe.start();



        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
