package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EventosListaActivity extends AppCompatActivity {


    private RecyclerView rv;
    private List<Evento> ListaEventos;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;

    String nombreUser;
    Button btnActualiza;
    Button btnNuevoEvento;

    String error = "no";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_lista);

        rv=(RecyclerView)findViewById(R.id.rvlista_eventos);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        initializeData();


        btnActualiza = (Button) findViewById(R.id.btn_actualiza_lista_eventos);
        btnNuevoEvento = (Button) findViewById(R.id.btn_nuevo_evento);

        btnActualiza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ListaEventos = new ArrayList<Evento>();
                ListaEventos.clear();
                rv = null;
                rv=(RecyclerView)findViewById(R.id.rvlista_eventos);
                LinearLayoutManager llm = new LinearLayoutManager(EventosListaActivity.this);
                rv.setLayoutManager(llm);
                rv.setHasFixedSize(true);
                //initializeAdapter();
                initializeData();
            }
        });

        btnNuevoEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mcont, EventoNuevoActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static


                i.putExtra("usuario", nombreUser);

                mcont.startActivity(i);


            }
        });



        Bundle extras = getIntent().getExtras();

        nombreUser = extras.getString("usuario");

    }


    @Override
    protected void onResume() {
        super.onResume();

        ListaEventos = new ArrayList<Evento>();

        ListaEventos.clear();

        rv = null;
        rv=(RecyclerView)findViewById(R.id.rvlista_eventos);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        initializeData();
        //initializeAdapter();


    }

    private void initializeData(){

        showList();
    }
    private void initializeAdapter(){
        RVAdapterListaEventos adapter = new RVAdapterListaEventos(ListaEventos, mcont, nombreUser);

        rv.setAdapter(adapter);
    }


    private String desencriptar(String criptograma, SecretKey clave) throws IOException {
        try {
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, clave);
            byte[] decVal = new Base64().decode(criptograma.getBytes("UTF-8"));

            byte[] bmensaje = c.doFinal(decVal);
            return new String(bmensaje, "UTF-8");
        } catch (Exception e) {
           // mensaje = null;
            e.printStackTrace();
           // elSocket.close();
            //System.out.println("Error al desencriptar");
            return null;
        }
    }


    private void showList() {
        class CrearLista extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    try {
                        elsocket = new Socket(HOST, PUERTO);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    ListaEventos = null;

                    PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                    pw.println("muestraListaEventos19930914-" + nombreUser + "-1993End09Of14Stream-");

                    InputStreamReader lectorListaEventos = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                    BufferedReader brListaEventos = new BufferedReader(lectorListaEventos);
                    String resultado = brListaEventos.readLine();
                    //System.out.println(listado);


                        byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");

                        MessageDigest sha;

                        sha = MessageDigest.getInstance("SHA-1");

                        miclaveBits = sha.digest(miclaveBits);

                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

                        String listado = desencriptar(resultado, claveTrans);





                    String[] eventosSinOrdenar = listado.split("-1993SeparadorGrande14-");


                    if(listado.equals("-nodata-")){



                        cancel(true);
                    }else{

                    ListaEventos = new ArrayList<Evento>();

                        for(int i = 0; i < eventosSinOrdenar.length; i++) {

                            try {
                                String[] singleEvent;

                                singleEvent = eventosSinOrdenar[i].split("-1993Separador14-");

                                //System.out.println(tempListaClanComentario[0] + tempListaClanComentario[1]);

                                String[] singleEventDescrip = singleEvent[1].split("-1993Separador214-");

                                Evento even = new Evento(singleEvent[0], singleEventDescrip[0], singleEventDescrip[1]);

                                //System.out.println(listaClanComentario[i]);

                                ListaEventos.add(even);
                            }catch(Exception ie){
                                ListaEventos.clear();


                                cancel(true);
                            }
                            //System.out.println(listaClanComentario[i]);
                        }
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    Intent i = new Intent(mcont, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.

                    mcont.startActivity(i);

                    error = "si";//Solo debe saltar cuando el error es fatal.

                    cancel(true);

                }
                return null;
            }


            @Override
            protected void onCancelled() {
                super.onCancelled();


                if(error.equals("si")) {
                    Toast.makeText(mcont, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(mcont, "Conexion interrumpida", Toast.LENGTH_SHORT).show();
                    initializeAdapter();
                }
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                initializeAdapter();

            }
        }
        new CrearLista().execute();

    }
}
