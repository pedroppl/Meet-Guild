package com.project.pablo.meetguild;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static java.lang.Thread.sleep;
import static java.lang.Thread.yield;

public class g_sign_SignInActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private static final String TAG = "g_sign_SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;
    public static String tempAccId = "";

    String HOST = "192.168.1.137";
    int PUERTO = 14024;
    public static Socket elsocket ;

    boolean connectionError = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.g_sign_activity_main);

        // Views
        mStatusTextView = (TextView) findViewById(R.id.status);

        // Button listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        findViewById(R.id.disconnect_button).setOnClickListener(this);


        String serverID = getString(R.string.server_client_id);

        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
               // .requestIdToken("604309838890-d60lnme0f8eobrskvd2r4ln0llqdjcpg.apps.googleusercontent.com")
               // .requestServerAuthCode(serverID, false)
                .requestIdToken(serverID)
                .requestEmail()
                .build();




        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

        // [START customize_button]
        // Set the dimensions of the sign-in button.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        // [END customize_button]



        //Tratamos de iniciar sesion automaticamente. Si no habia una sesion iniciada, se cargará el gestor de cuentas.
        signIn();

    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                   // hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    // [END onActivityResult]






    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
           // mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //mStatusTextView.append(" Correo: " + acct.getEmail());
            //mStatusTextView.append("\n ID: " + acct.getIdToken());


            tempAccId = acct.getIdToken();


            //System.out.println("ID LOGIN --> " + tempAccId);

            updateUI(true);

            //Toast.makeText(this, "Login exitoso google", Toast.LENGTH_SHORT).show();


            //Comunicacion con el servidor. el codigo que representa el login para comprobarlo en el server.
            //El servidor comprueba que el login ha sido el correcto, obtiene el correo y comprueba si el correo ya estaba en la base de datos.
            //Si no está, mostrar la activity de la lista de guilds.
            //Si está, mostrar la activity de su clan. Los datos se obtienen del servidor y son pedidos de nuevo a este por medio de API REST.



             class CrearConexiones extends AsyncTask<Void, Void, Void> {


                 @Override
                 protected void onPreExecute() {
                     super.onPreExecute();


                 }

                 @Override
                 protected Void doInBackground(Void... voids) {


                     try {
                         elsocket = new Socket(HOST, PUERTO);
                     } catch (IOException e) {
                         e.printStackTrace();
                         System.out.println("Error durante la conexion con el servidor");
                         connectionError = true;
                     }
                     try {
                         new Conectador(elsocket);
                     } catch (Exception e) {
                         e.printStackTrace();
                         System.out.println("Error durante la conexion con el servidor");
                         connectionError = true;
                     }


                     return null;

                 }



                 @Override
                 protected void onPostExecute(Void aVoid) {
                     super.onPostExecute(aVoid);

                     if(connectionError){
                         Toast.makeText(g_sign_SignInActivity.this, "NO TRADUCIDO: ERROR DE CONEXION", Toast.LENGTH_SHORT).show();
                         finish();
                     }


                     try {
                         if (Conectador.respuestaServer.equals("user_no_registrado") || Conectador.respuestaServer.equals("user_registrado_no_name")) {
                             //lanzamos la activity para el registro del user name y la guild.

                             connectionError = false;

                             hideProgressDialog();

                             Intent i = new Intent(getApplicationContext(), UserNameChooserActivity.class);

                             i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                             getApplicationContext().startActivity(i);

                         }else if(Conectador.respuestaServer.equals("user_registrado_no_guild") ){
                             connectionError = false;

                             hideProgressDialog();

                             Intent i = new Intent(getApplicationContext(), MainActivity.class);

                             i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                             getApplicationContext().startActivity(i);
                         }else if(Conectador.respuestaServer.equals("user_registrado")){

                             connectionError = false;

                             class recibeDatosCliente extends Thread {

                                 @Override
                                 public void run() {

                                     try {
                                         System.out.println("Esperando user y guild");


                                         System.out.println("Recibido user y guild, intentando lanzar ventana GUILD");

                                         InputStreamReader lectorGetUserNameAndGuild = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");

                                         BufferedReader bruserNameAndGuild = new BufferedReader(lectorGetUserNameAndGuild);
                                         String respuesta = bruserNameAndGuild.readLine();


                                         String[] userAndGuild = respuesta.split("-19930914-");

                                         String user = userAndGuild[0];
                                         String guild = userAndGuild[1];


                                         Intent i = new Intent(getApplicationContext(), Guild_interface.class);

                                         i.putExtra("user", user);
                                         i.putExtra("guild", guild);

                                         i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                                         getApplicationContext().startActivity(i);


                                     } catch (Exception e) {

                                     }
                                 }
                             }

                             new recibeDatosCliente().start();
                         }

                     }catch (Exception e){

                         connectionError = true;

                         e.printStackTrace();

                         hideProgressDialog();

                     }
                 }


            }


            new CrearConexiones().execute();
           // hideProgressDialog();


            finish();



            //Se supone que si llegamos hasta aqui, es que el servidor ha enviado la rspuesta, el cliente la tiene y vamos a usarla.




/*
            Intent i = new Intent(getApplicationContext(), MainActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

            getApplicationContext().startActivity(i);

        */





            //finish();
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Toast.makeText(getApplicationContext(), "Sign Out: " + status, Toast.LENGTH_SHORT).show();
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Toast.makeText(getApplicationContext(), "Revoke access: " + status, Toast.LENGTH_SHORT).show();
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]


    @Override
    protected void onDestroy() {
        super.onDestroy();



        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {

            mProgressDialog.hide();

        }
    }

    private void updateUI(boolean signedIn) {

        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        //findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        /*
        if (signedIn) {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
        */
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.disconnect_button:
                revokeAccess();
                break;
        }
    }
}
