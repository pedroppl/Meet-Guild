package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;



public class RVAdapterLider extends RecyclerView.Adapter<RVAdapterLider.SolicitudLiderViewHolder> {


    static Context mcontext;
    static String lidername;


    public static class SolicitudLiderViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nombreUsuario;
        TextView solicitudLider;
        String estado;

        SolicitudLiderViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            nombreUsuario = (TextView)itemView.findViewById(R.id.txt_nombreUsuario);
            solicitudLider = (TextView)itemView.findViewById(R.id.txt_solicitud_lider);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (estado.equals("rechazada")) {
                        Toast.makeText(mcontext, R.string.solicitud_lider_ya_rechazada, Toast.LENGTH_SHORT).show();
                    } else if (estado.equals("aceptada")) {


                        Toast.makeText(mcontext, R.string.solicitud_lider_ya_aceptada, Toast.LENGTH_SHORT).show();

                    } else {

                        Intent i = new Intent(mcontext, SolicitudLiderActivity.class);

                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                        i.putExtra("nombreUsuario", nombreUsuario.getText().toString());
                        i.putExtra("textoSolicitud", solicitudLider.getText().toString());
                        i.putExtra("nombreLider", lidername);

                        mcontext.startActivity(i);

                        //Toast.makeText(mcontext, "No implementado", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    List<SolicitudLider> SolicitudesLider;

    RVAdapterLider(List<SolicitudLider> SolicitudesLider, Context ctx, String nombreLider){
        this.SolicitudesLider = SolicitudesLider;

        mcontext = ctx;//Recuperamos el contexto y lo almacenamos.

        lidername = nombreLider;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public SolicitudLiderViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_solicitud_lider, viewGroup, false);
        SolicitudLiderViewHolder pvh = new SolicitudLiderViewHolder(v);



        return pvh;
    }

    @Override
    public void onBindViewHolder(SolicitudLiderViewHolder solicitudViewHolder, int i) {
        solicitudViewHolder.nombreUsuario.setText(SolicitudesLider.get(i).nombre_usuario);
        solicitudViewHolder.solicitudLider.setText(SolicitudesLider.get(i).comentario);
        solicitudViewHolder.estado = SolicitudesLider.get(i).estado;


    }

    @Override
    public int getItemCount() {

        try {
            return SolicitudesLider.size();
        }catch(Exception e) {
            return 0;
        }
    }

}
