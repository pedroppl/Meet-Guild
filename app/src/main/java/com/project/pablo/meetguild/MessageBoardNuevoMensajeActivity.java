package com.project.pablo.meetguild;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.regex.Pattern;

public class MessageBoardNuevoMensajeActivity extends AppCompatActivity {


    Switch incluyeWeb;
    EditText edtextweb;
    Button btnenviar;
    EditText texto;


    String nombreUser;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;
    String sweb;
    String stexto;
    String mensaje = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_board_nuevo_mensaje);


        Bundle extras = getIntent().getExtras();

        nombreUser = extras.getString("usuario");


        incluyeWeb = (Switch) findViewById(R.id.sw_incluir_url);
        edtextweb = (EditText) findViewById(R.id.edtxt_url_nuevo_mensaje);
        btnenviar = (Button) findViewById(R.id.btn_enviar_nuevo_mensaje);
        texto = (EditText) findViewById(R.id.edtxt_texto_mensaje);

        incluyeWeb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    edtextweb.setVisibility(View.VISIBLE);
                    edtextweb.setText("");
                }else{
                    edtextweb.setVisibility(View.GONE);
                    edtextweb.setText("no_web");
                }
            }
        });

        btnenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edtextweb.getText().toString().isEmpty()){
                    sweb = "no_web";
                }else{
                    sweb = edtextweb.getText().toString();
                    sweb =  sweb.trim();
                }
                stexto = texto.getText().toString();





                class EnviaMensaje extends AsyncTask<Void, Void, Void> {
                    @Override
                    protected Void doInBackground(Void... voids) {

                        try {
                            try {
                                elsocket = new Socket(HOST, PUERTO);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                            pw.println("-creanuevomensaje19930914-" + nombreUser + "-1993separador14-"+ stexto + "-1993separadorWeb14-" +  sweb + "-1993End09Of14Stream-");

                            InputStreamReader lectorRespuestaMensaje = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                            BufferedReader brRespuestaMensaje = new BufferedReader(lectorRespuestaMensaje);
                            mensaje = brRespuestaMensaje.readLine();


                        }catch(Exception e){

                            e.printStackTrace();

                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        if(mensaje.contains("mensaje_ok")){
                            Toast.makeText(mcont, R.string.mensaje_publicado_si, Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(mcont, R.string.mensaje_publicado_no, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                new EnviaMensaje().execute();

            }
        });



        /*
        String mclick = "Pinchame";
        String murl = "google.es";
        TextView noteView = (TextView) findViewById(R.id.txt_mensaje_titulofijo);
        noteView.setText("Mirad lo que he encontrado, es muy util " + mclick);
        //Linkify.addLinks(noteView, Linkify.ALL);

        Pattern prueba = Pattern.compile("\\b(Pinchame)\\b");

        Linkify.addLinks(noteView, prueba, murl);

        */
        //Si queremos poner un enlace adicional, activamos una casilla para poner el enlace.
        //A la hora de construir el texto, en vez de poner el enlace, ponmemos "pinchame!"
        //Entonces, Linkify buscará cada vez que encuentre la palabra "pinchame!" y activará para que se convierta en un enlace que apunta a google.es

    }


}
