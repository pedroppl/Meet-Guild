package com.project.pablo.meetguild;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

//Revisar nombres de variables.

public class RVAdapterUsuario extends RecyclerView.Adapter<RVAdapterUsuario.SolicitudUsuarioViewHolder> {


    static Context mcontext;


    public static class SolicitudUsuarioViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nombreGuild;
        TextView descripcionSolicitud;//Probablemente esto sea la descripcion
        String estado;

        SolicitudUsuarioViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvusuario);
            nombreGuild = (TextView)itemView.findViewById(R.id.txt_nombreGuild);
            descripcionSolicitud = (TextView)itemView.findViewById(R.id.txt_solicitud_usuario);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(estado.equals("rechazada")){
                        Toast.makeText(mcontext, R.string.solicitud_usuario_rechazada, Toast.LENGTH_SHORT).show();
                    }else {
                        if(estado.equals("aceptada")){
                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:
                                            //Yes button clicked
                                            Toast.makeText(mcontext, R.string.solicitud_usuario_aceptada, Toast.LENGTH_SHORT).show();

                                            class AceptarSolicitudLider extends AsyncTask<Void, Void, Void> {

                                                private ProgressDialog mProgressDialog;
                                                String Respuesta;

                                                String nombreDeGuild;
                                                //String nombreUser = nombreUsuario.getText().toString();
                                                private void showProgressDialog() {
                                                    if (mProgressDialog == null) {
                                                        mProgressDialog = new ProgressDialog(mcontext);
                                                        mProgressDialog.setMessage(mcontext.getString(R.string.loading));
                                                        mProgressDialog.setIndeterminate(true);
                                                    }

                                                    mProgressDialog.show();
                                                }

                                                private void hideProgressDialog() {
                                                    if (mProgressDialog != null && mProgressDialog.isShowing()) {

                                                        mProgressDialog.hide();

                                                        mProgressDialog.dismiss();
                                                        mProgressDialog = null;
                                                    }
                                                }


                                                @Override
                                                protected void onPostExecute(Void aVoid) {
                                                    super.onPostExecute(aVoid);

                                                    hideProgressDialog();

                                                    String[] userAndGuild = Respuesta.split("-19930914-");

                                                    String user = userAndGuild[0];
                                                    String guild = userAndGuild[1];
                                                    System.out.println(user + "---------- " + guild);

                                                    Intent i = new Intent(mcontext, Guild_interface.class);

                                                    i.putExtra("user", user);
                                                    i.putExtra("guild", guild);

                                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    mcontext.startActivity(i);
                                                }

                                                @Override
                                                protected Void doInBackground(Void... voids) {

                                                    try {


                                                        //FALTA CIFRAR INFORMACION


                                                        // Toast.makeText(mcontext, "Empezando conexion", Toast.LENGTH_SHORT).show();

                                                        //elsocket = new Socket(HOST, PUERTO);
                                                        PrintWriter pw = new PrintWriter(Conectador.skcli.getOutputStream(), true);
                                                        pw.println("-1993SolicitudAceptada0914-"+ nombreDeGuild);

                                                        InputStreamReader lectorSolicitudLider = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");
                                                        BufferedReader brSolicitudLider = new BufferedReader(lectorSolicitudLider);
                                                        Respuesta = brSolicitudLider.readLine();

                                                    }catch(Exception e){
                                                        e.printStackTrace();
                                                        //Toast.makeText(mcontext, "Error", Toast.LENGTH_SHORT).show();


                                                        Intent i = new Intent(mcontext, LoginActivity.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.


                                                        mcontext.startActivity(i);
                                                        Toast.makeText(mcontext, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();


                                                    }
                                                    return null;
                                                }

                                                @Override
                                                protected void onPreExecute() {
                                                    super.onPreExecute();

                                                    nombreDeGuild = nombreGuild.getText().toString();

                                                    showProgressDialog();

                                                }
                                            }

                                            new AceptarSolicitudLider().execute();



                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            //Toast.makeText(mcontext, "Solicitud NO aceptada", Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
                            builder.setMessage(R.string.solicitud_usuario_pregunta).setPositiveButton(R.string.solicitud_usuario_respuesta1, dialogClickListener)
                                    .setNegativeButton(R.string.solicitud_usuario_respuesta2, dialogClickListener).show();



                        }else {

                           // Toast.makeText(mcontext, "No implementado", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
        }



    }

    List<SolicitudUsuario> SolicitudesUsuario;

    RVAdapterUsuario(List<SolicitudUsuario> SolicitudesUsuario, Context ctx){
        this.SolicitudesUsuario = SolicitudesUsuario;

        mcontext = ctx;//Recuperamos el contexto y lo almacenamos.


    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public SolicitudUsuarioViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_solicitud_usuario, viewGroup, false);
        SolicitudUsuarioViewHolder svh = new SolicitudUsuarioViewHolder(v);



        return svh;
    }

    @Override
    public void onBindViewHolder(SolicitudUsuarioViewHolder SolicitudViewHolder, int i) {
        SolicitudViewHolder.nombreGuild.setText(SolicitudesUsuario.get(i).nombre_guild);
        SolicitudViewHolder.descripcionSolicitud.setText(SolicitudesUsuario.get(i).comentario);
        SolicitudViewHolder.estado = SolicitudesUsuario.get(i).estado;



    }

    @Override
    public int getItemCount() {
        try {
            return SolicitudesUsuario.size();
        }catch(Exception e){
            return 0;
        }

    }



}
