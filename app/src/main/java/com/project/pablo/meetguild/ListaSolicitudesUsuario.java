package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ListaSolicitudesUsuario extends AppCompatActivity {



    private RecyclerView rv;
    private List<SolicitudUsuario> SolicitudesUsuario;
    private Context mcont = this;
    String error = "no";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_solicitudes_usuario);

        rv=(RecyclerView)findViewById(R.id.rvusuario);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        //URL = "http://192.168.1.137/meetguild/api/allguilds";
        initializeData();

    }


    private void initializeData(){

        showList();
    }
    private void initializeAdapter(){
        RVAdapterUsuario adapter = new RVAdapterUsuario(SolicitudesUsuario, mcont);

        rv.setAdapter(adapter);
    }

    private String desencriptar(String criptograma, SecretKey clave) throws IOException {
        try {
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, clave);
            byte[] decVal = new Base64().decode(criptograma.getBytes("UTF-8"));

            byte[] bmensaje = c.doFinal(decVal);
            return new String(bmensaje, "UTF-8");
        } catch (Exception e) {

            e.printStackTrace();

            //System.out.println("Error al desencriptar");
            return null;
        }
    }


    private void showList() {


        class CrearLista extends AsyncTask<Void, Void, Void> {


            @Override
            protected void onCancelled() {
                super.onCancelled();

                if(error.equals("si")) {
                    Toast.makeText(mcont, "Ha ocurrido un error, desconectando por seguridad.", Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(mcont, "Conexion interrumpida", Toast.LENGTH_SHORT).show();
                    initializeAdapter();
                }


            }

            @Override
            protected Void doInBackground(Void... voids) {

                try {

                    PrintWriter pw = new PrintWriter(Conectador.skcli.getOutputStream(), true);
                    pw.println("muestraSolicitudesUsuario19930914-");

                    InputStreamReader lectorListaSolicitudesUsuario = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");
                    BufferedReader brListaSolicitudesUsuario = new BufferedReader(lectorListaSolicitudesUsuario);

                    String resultado = brListaSolicitudesUsuario.readLine();

                        byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");

                        MessageDigest sha;

                        sha = MessageDigest.getInstance("SHA-1");

                        miclaveBits = sha.digest(miclaveBits);

                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

                        String listado = desencriptar(resultado, claveTrans);



                    String[] listaClanComentario = listado.split("-1993SeparadorGrande14-");

                    SolicitudesUsuario = new ArrayList<SolicitudUsuario>();

                    if(listado.equals("-nodata-")){
                        cancel(true);
                    }else {

                        for (int i = 0; i < listaClanComentario.length; i++) {

                            String[] tempListaClanComentario;

                            tempListaClanComentario = listaClanComentario[i].split("-1993Separador14-");

                            //System.out.println(tempListaClanComentario[0] + tempListaClanComentario[1]);

                            String[] tempListaComentarioEstado = tempListaClanComentario[1].split("-1993Separador214-");

                            SolicitudUsuario soli = new SolicitudUsuario(tempListaClanComentario[0], tempListaComentarioEstado[0], tempListaComentarioEstado[1]);

                            SolicitudesUsuario.add(soli);

                            //System.out.println(listaClanComentario[i]);

                        }
                    }

                } catch (Exception e) {

                    e.printStackTrace();


                    Intent i = new Intent(mcont, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.

                    error = "si";//Solo debe saltar cuando el error es fatal.

                    mcont.startActivity(i);
                    Toast.makeText(mcont, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();



                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                initializeAdapter();

            }
        }

        new CrearLista().execute();
    }
}




