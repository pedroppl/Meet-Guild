package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

//Revisar nombres de variables.

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {


    static Context mcontext;



    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nombreGuild;
        TextView descripcionGuild;//Probablemente esto sea la descripcion
        ImageView imagenGuild;


        PersonViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            nombreGuild = (TextView)itemView.findViewById(R.id.guild_name);
            descripcionGuild = (TextView)itemView.findViewById(R.id.guild_desc);
            //imagenGuild = (ImageView)itemView.findViewById(R.id.guild_photo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    Intent i = new Intent(mcontext, SolicitudIngresoCreator.class);


                    i.putExtra("guild", nombreGuild.getText().toString());

                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                    mcontext.startActivity(i);

                   // Toast.makeText(mcontext, nombreGuild.getText().toString(), Toast.LENGTH_SHORT).show();

                }
            });
        }



    }

    List<Guild> guilds;

    RVAdapter(List<Guild> guilds, Context ctx){
        this.guilds = guilds;

        mcontext = ctx;//Recuperamos el contexto y lo almacenamos.


    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);



        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.nombreGuild.setText(guilds.get(i).nombre_guild);
        personViewHolder.descripcionGuild.setText(guilds.get(i).descripcion);
        //personViewHolder.personPhoto.setImageResource(guilds.get(i).photoId);



    }

    @Override
    public int getItemCount() {
        return guilds.size();
    }



}
