package com.project.pablo.meetguild;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class EventoSingleEventActivity extends AppCompatActivity {


    TextView fecha;
    TextView titulo;
    TextView texto;
    Button btn_borrar;
    String nombreuser;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento_single_event);


        fecha = (TextView) findViewById(R.id.txt_fecha_single_event);
        titulo = (TextView) findViewById(R.id.txt_titulo_single_event);
        texto = (TextView) findViewById(R.id.txt_texto_single_event);

        btn_borrar = (Button) findViewById(R.id.btn_borrar_evento);


        Bundle extras = getIntent().getExtras();

        fecha.setText(extras.getString("fecha"));
        titulo.setText(extras.getString("titulo"));
        texto.setText(extras.getString("texto"));
        nombreuser = extras.getString("username");

        btn_borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked

                                class BorraEvento extends AsyncTask<Void, Void, Void> {


                                    String sfecha = fecha.getText().toString();
                                    String stitulo = titulo.getText().toString();
                                    String stexto = texto.getText().toString();
                                    String mensaje = null;
                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        try {
                                            try {
                                                elsocket = new Socket(HOST, PUERTO);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }

                                            PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                                            pw.println("-borraEvento19930914-" + nombreuser+"-1993separador14-"+sfecha+"-1993separador14-"+stitulo+"-1993separador14-"+stexto + "-1993End09Of14Stream-");

                                            InputStreamReader lectorSingleEvent = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                                            BufferedReader brSingleEvent = new BufferedReader(lectorSingleEvent);
                                            mensaje = brSingleEvent.readLine();



                                        } catch (Exception e) {

                                            e.printStackTrace();

                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        super.onPostExecute(aVoid);

                                        if(mensaje.contains("usuario_no_lider")){
                                            Toast.makeText(mcont, getResources().getString(R.string.toast_error_no_lider), Toast.LENGTH_SHORT).show();
                                        }
                                        if(mensaje.contains("evento_borrado")){
                                            //Toast.makeText(mcont, "Evento borrado", Toast.LENGTH_SHORT).show();

                                            finish();

                                        }

                                    }
                                }




                                new BorraEvento().execute();
                                //Debemos comprobar que el usuario es lider, y despues borrar el mensaje.
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                //Toast.makeText(mcontext, "Solicitud NO aceptada", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                };



                AlertDialog.Builder builder = new AlertDialog.Builder(EventoSingleEventActivity.this);
                builder.setMessage("¿Borrar evento?").setPositiveButton("Aceptar", dialogClickListener)
                        .setNegativeButton("Cancelar", dialogClickListener).show();
            }
        });




    }
}
