package com.project.pablo.meetguild;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.widget.Toast;

import java.io.PrintWriter;
import java.net.Socket;


public class EnviadorPing extends Service {


    static Socket skSRV;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        System.out.println("INICIANDO SERVICIO PING SENDER");

        skSRV = Conectador.skcli;


        Thread simpleThread = new Thread() {
            @Override
            public void run() {

                Boolean escape = true;

                while (escape) {

                    try {

                        PrintWriter pw = new PrintWriter(skSRV.getOutputStream(), true);
                        pw.println("-1993stay-alive0914-");
                        sleep(60000);
                    }catch(Exception e){

                        Guild_interface.hayErrorConexion = true;


                        //Toast.makeText(EnviadorPing.this, "NO TRADUCIDO: Error de conexion", Toast.LENGTH_SHORT).show();
                        System.err.println("Error al enviar ping");
                        escape = false;
                        stopSelf();



                    }
                }

            }
        };

        simpleThread.start();

        return super.onStartCommand(intent, flags, startId);
    }


}
