package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ListaSolicitudesLider extends AppCompatActivity {

    private RecyclerView rv;
    private List<SolicitudLider> SolicitudesLider;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;
    String leaderUserName;
    String error = "no";

    Button btnactualiza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_solicitudes_lider);


        rv=(RecyclerView)findViewById(R.id.rvLider);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        Bundle extras = getIntent().getExtras();

        leaderUserName = extras.getString("usuario");

        btnactualiza = (Button) findViewById(R.id.btn_actualiza_solicitudes_lider);

        btnactualiza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SolicitudesLider = new ArrayList<SolicitudLider>();
                SolicitudesLider.clear();
                rv = null;
                rv=(RecyclerView)findViewById(R.id.rvLider);
                LinearLayoutManager llm = new LinearLayoutManager(ListaSolicitudesLider.this);
                rv.setLayoutManager(llm);
                rv.setHasFixedSize(true);
                //initializeAdapter();
                initializeData();
            }
        });





        initializeData();
    }

    private void initializeData(){

        showList();
    }
    private void initializeAdapter(){
        RVAdapterLider adapter = new RVAdapterLider(SolicitudesLider, mcont, leaderUserName);

        rv.setAdapter(adapter);
    }


    private String desencriptar(String criptograma, SecretKey clave) throws IOException {
        try {
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, clave);
            byte[] decVal = new Base64().decode(criptograma.getBytes("UTF-8"));

            byte[] bmensaje = c.doFinal(decVal);
            return new String(bmensaje, "UTF-8");
        } catch (Exception e) {
            //mensaje = null;
            e.printStackTrace();
           // elSocket.close();
            //System.out.println("Error al desencriptar");
            return null;
        }
    }



    private void showList() {

        class CrearLista extends AsyncTask<Void, Void, Void> {


            @Override
            protected void onCancelled() {
                super.onCancelled();

                if(error.equals("si")) {
                    Toast.makeText(mcont, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(mcont, "Conexion interrumpida", Toast.LENGTH_SHORT).show();
                    initializeAdapter();
                }


            }

            @Override
            protected Void doInBackground(Void... voids) {

                try {

                    try {
                        elsocket = new Socket(HOST, PUERTO);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                    pw.println("-enviaListaSolicitudes1993-"+leaderUserName  + "-1993End09Of14Stream-");

                    InputStreamReader lectorListaSolicitudesUsuario = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                    BufferedReader brListaSolicitudesUsuario = new BufferedReader(lectorListaSolicitudesUsuario);

                    String resultado = brListaSolicitudesUsuario.readLine();

                        byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");

                        MessageDigest sha;

                        sha = MessageDigest.getInstance("SHA-1");

                        miclaveBits = sha.digest(miclaveBits);

                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

                    String listado = desencriptar(resultado, claveTrans);
                    //System.out.println(listado);

                    if(listado.equals("-nodata-")){
                        cancel(true);
                    }else {


                        String[] listaClanComentario = listado.split("-1993SeparadorGrande14-");


                        SolicitudesLider = new ArrayList<SolicitudLider>();

                        for (int i = 0; i < listaClanComentario.length; i++) {

                            String[] tempListaClanComentario;

                            tempListaClanComentario = listaClanComentario[i].split("-1993Separador14-");

                            //System.out.println(tempListaClanComentario[0] + tempListaClanComentario[1]);

                            String[] tempListaComentarioEstado = tempListaClanComentario[1].split("-1993Separador214-");

                            SolicitudLider soli = new SolicitudLider(tempListaClanComentario[0], tempListaComentarioEstado[0], tempListaComentarioEstado[1]);

                            SolicitudesLider.add(soli);

                            //System.out.println(listaClanComentario[i]);

                        }
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    Intent i = new Intent(mcont, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.

                    error = "si";//Solo debe saltar cuando el error es fatal.


                    mcont.startActivity(i);
//                    Toast.makeText(mcont, "Ha ocurrido un error, desconectando por seguridad.", Toast.LENGTH_SHORT).show();



                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                initializeAdapter();

            }
        }

        new CrearLista().execute();
    }


}
