package com.project.pablo.meetguild;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EventoNuevoActivity extends AppCompatActivity {



    Button btnenviar;
    String reportDate = "";
    EditText titulo;
    EditText texto;

    String nombreUser;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento_nuevo);

        //Se permite que se creen eventos con fecha anterior a la actual, debido a que la guild pudiera querer llevar un registro de eventos pasados antes de ingresar en la aplicacion.

        SingleDateAndTimePicker singleDateAndTimePicker = (SingleDateAndTimePicker) findViewById(R.id.cstm_fecha_evento);
        btnenviar = (Button) findViewById(R.id.btn_enviar_evento);

        titulo = (EditText) findViewById(R.id.edtxt_titulo_evento);
        texto = (EditText) findViewById(R.id.edtxt_texto_evento);

        Bundle extras = getIntent().getExtras();

        nombreUser = extras.getString("usuario");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        reportDate = df.format(singleDateAndTimePicker.getDate());

        btnenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                
                if(texto.getText().toString().isEmpty()){
                    Toast.makeText(mcont, getResources().getString(R.string.toast_error_campo_vacio), Toast.LENGTH_SHORT).show();
                }else {


                    class EnviaEvento extends AsyncTask<Void, Void, Void> {

                        String stitulo = titulo.getText().toString();
                        String stexto = texto.getText().toString();
                        String mensaje;

                        @Override
                        protected Void doInBackground(Void... voids) {

                            try {
                                try {
                                    elsocket = new Socket(HOST, PUERTO);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                                pw.println("-creanuevoevento19930914-" + nombreUser + "-1993separador14-" + reportDate + "-1993separador14-" + stitulo + "-1993separador14-" + stexto + "-1993End09Of14Stream-");

                                InputStreamReader lectorListaEventos = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                                BufferedReader brListaEventos = new BufferedReader(lectorListaEventos);
                                mensaje = brListaEventos.readLine();

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            if (mensaje.equals("usuario_no_lider")) {
                                Toast.makeText(mcont, getResources().getString(R.string.toast_error_no_lider), Toast.LENGTH_SHORT).show();
                                finish();
                            }

                            if (mensaje.equals("evento insertado correctamente")) {
                                Toast.makeText(mcont, getResources().getString(R.string.eventos_evento_publicado), Toast.LENGTH_SHORT).show();
                            }

                            finish();

                        }
                    }

                    new EnviaEvento().execute();

                }

            }
        });


        singleDateAndTimePicker.setListener(new SingleDateAndTimePicker.Listener(){

            @Override
            public void onDateChanged(String displayed, Date date) {

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                reportDate = df.format(date);

                //System.out.println("La fecha a String es: " + reportDate);

              //  Toast.makeText(EventoNuevoActivity.this, "la fecha a String es: " + reportDate, Toast.LENGTH_SHORT).show();
                
                
            }
        });



    }
}
