package com.project.pablo.meetguild;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SolicitudLiderActivity extends AppCompatActivity {

    TextView nombreUsuario;
    TextView textoSolicitud;
    Button aceptarSolicitud;
    Button rechazarSolicitud;

    Context mcontext;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;
    String nombreLider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitud_lider);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        nombreUsuario = (TextView) findViewById(R.id.txt_nombre_usuario_solicitud_lider);
        textoSolicitud = (TextView) findViewById(R.id.txt_texto_solicitud_lider);

        Bundle extras = getIntent().getExtras();

        nombreUsuario.setText(extras.getString("nombreUsuario"));
        textoSolicitud.setText(extras.getString("textoSolicitud"));

        nombreLider = extras.getString("nombreLider");

        aceptarSolicitud = (Button) findViewById(R.id.btn_aceptar_solicitud_lider);
        rechazarSolicitud = (Button) findViewById(R.id.btn_rechazar_solicitud_lider);

        mcontext = this;

        aceptarSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Toast.makeText(mcontext, "Boton pulsado", Toast.LENGTH_SHORT).show();

                 class AceptaSolicitudLider extends AsyncTask<Void, Void, Void> {

                     private ProgressDialog mProgressDialog;
                     String Respuesta;

                     String nombreUser = nombreUsuario.getText().toString();
                     private void showProgressDialog() {
                         if (mProgressDialog == null) {
                             mProgressDialog = new ProgressDialog(mcontext);
                             mProgressDialog.setMessage(getString(R.string.loading));
                             mProgressDialog.setIndeterminate(true);
                         }

                         mProgressDialog.show();
                     }

                     private void hideProgressDialog() {
                         if (mProgressDialog != null && mProgressDialog.isShowing()) {

                             mProgressDialog.hide();

                         }
                     }

                     @Override
                     protected void onPostExecute(Void aVoid) {
                         super.onPostExecute(aVoid);

                         hideProgressDialog();

                         if(Respuesta.contains("Actualizado correctamente.")){

                             Toast.makeText(mcontext, "Actualizado correctamente", Toast.LENGTH_SHORT).show();
                         }else if(Respuesta.contains("user-no-lider")){

                             Toast.makeText(mcontext, R.string.toast_error_no_lider, Toast.LENGTH_SHORT).show();
                         }

                         finish();

                     }

                     @Override
                     protected Void doInBackground(Void... voids) {

                         try {
                            // Toast.makeText(mcontext, "Empezando conexion", Toast.LENGTH_SHORT).show();

                             elsocket = new Socket(HOST, PUERTO);
                             PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                             pw.println("-solicitudLiderAceptada1993-"+nombreLider+"-19930914-" + nombreUser +"-1993Respuesta-"+"aceptada" + "-1993End09Of14Stream-");

                             InputStreamReader lectorSolicitudLider = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                             BufferedReader brSolicitudLider = new BufferedReader(lectorSolicitudLider);
                             Respuesta = brSolicitudLider.readLine();


                             //Toast.makeText(SolicitudLiderActivity.this, "Respuesta de servidor: " + Respuesta, Toast.LENGTH_SHORT).show();

                             //Hay que hacer esto con assyncTask, añadiendo un cuadro de dialogo de espera.
                             //SI el servidor  hace la tarea bien, borrar el elemento de la lista.

                         }catch(Exception e){
                             e.printStackTrace();
                             //Toast.makeText(mcontext, "Error", Toast.LENGTH_SHORT).show();

                             Intent i = new Intent(mcontext, LoginActivity.class);
                             i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                             i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.

                             mcontext.startActivity(i);
                             Toast.makeText(mcontext, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();



                         }
                         return null;
                     }

                     @Override
                     protected void onPreExecute() {
                         super.onPreExecute();

                         showProgressDialog();

                     }
                 }

                 new AceptaSolicitudLider().execute();
            }
        });



        rechazarSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(mcontext, "Boton pulsado", Toast.LENGTH_SHORT).show();

                class RechazaSolicitudLider extends AsyncTask<Void, Void, Void> {

                    private ProgressDialog mProgressDialog;
                    String Respuesta;

                    String nombreUser = nombreUsuario.getText().toString();
                    private void showProgressDialog() {
                        if (mProgressDialog == null) {
                            mProgressDialog = new ProgressDialog(mcontext);
                            mProgressDialog.setMessage(getString(R.string.loading));
                            mProgressDialog.setIndeterminate(true);
                        }

                        mProgressDialog.show();
                    }

                    private void hideProgressDialog() {
                        if (mProgressDialog != null && mProgressDialog.isShowing()) {

                            mProgressDialog.hide();

                            mProgressDialog.dismiss();
                            mProgressDialog = null;
                        }
                    }


                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        hideProgressDialog();

                        if(Respuesta.contains("Actualizado correctamente.")){

                            Toast.makeText(mcontext, R.string.solicitud_lider_actualizado_exito, Toast.LENGTH_SHORT).show();


                            finish();
                        }else if(Respuesta.contains("user-no-lider")){

                            Toast.makeText(mcontext, R.string.toast_error_no_lider, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {

                        try {
                            // Toast.makeText(mcontext, "Empezando conexion", Toast.LENGTH_SHORT).show();

                            elsocket = new Socket(HOST, PUERTO);
                            PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                            pw.println("-solicitudLiderDenegada1993-"+nombreLider+"-19930914-" + nombreUser +"-1993Respuesta-"+"rechazada" + "-1993End09Of14Stream-");

                            InputStreamReader lectorSolicitudLider = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                            BufferedReader brSolicitudLider = new BufferedReader(lectorSolicitudLider);
                            Respuesta = brSolicitudLider.readLine();

                        }catch(Exception e){
                            e.printStackTrace();
                            //Toast.makeText(mcontext, "Error", Toast.LENGTH_SHORT).show();

                        }
                        return null;
                    }

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();

                        showProgressDialog();

                    }
                }

                new RechazaSolicitudLider().execute();
            }
        });
    }
}
