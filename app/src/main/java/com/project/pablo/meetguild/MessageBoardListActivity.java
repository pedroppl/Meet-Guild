package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MessageBoardListActivity extends AppCompatActivity {


    private RecyclerView rv;
    private List<Mensaje> ListaMensajes;
    private Context mcont = this;
    String HOST = "192.168.1.137";
    int PUERTO = 14025;
    public static Socket elsocket ;
    String error = "no";

    String nombreUser;
    Button btnActualiza;
    Button btnNuevoMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_board_list);

        rv=(RecyclerView)findViewById(R.id.rvlista_mensajes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        btnActualiza = (Button) findViewById(R.id.btn_actualiza_lista_mensajes);
        btnNuevoMensaje = (Button) findViewById(R.id.btn_nuevo_mensaje);


        Bundle extras = getIntent().getExtras();

        nombreUser = extras.getString("usuario");

        btnActualiza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ListaMensajes = new ArrayList<Mensaje>();
                ListaMensajes.clear();
                rv = null;
                rv=(RecyclerView)findViewById(R.id.rvlista_mensajes);
                LinearLayoutManager llm = new LinearLayoutManager(MessageBoardListActivity.this);
                rv.setLayoutManager(llm);
                rv.setHasFixedSize(true);
                //initializeAdapter();
                initializeData();
            }
        });


        btnNuevoMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mcont, MessageBoardNuevoMensajeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                i.putExtra("usuario", nombreUser);
                mcont.startActivity(i);
            }
        });

        initializeData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        ListaMensajes = new ArrayList<Mensaje>();

        ListaMensajes.clear();

        rv = null;
        rv=(RecyclerView)findViewById(R.id.rvlista_mensajes);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        initializeData();
        //initializeAdapter();
    }

    private void initializeData(){
        showList();
    }
    private void initializeAdapter(){
        RVAdapterListaMensajes adapter = new RVAdapterListaMensajes(ListaMensajes, mcont, nombreUser);
        rv.setAdapter(adapter);
    }


    private String desencriptar(String criptograma, SecretKey clave) throws IOException {
        try {
            Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
            c.init(Cipher.DECRYPT_MODE, clave);
            byte[] decVal = new Base64().decode(criptograma.getBytes("UTF-8"));

            byte[] bmensaje = c.doFinal(decVal);
            return new String(bmensaje, "UTF-8");
        } catch (Exception e) {
           // mensaje = null;
            e.printStackTrace();
          //  elSocket.close();
            //System.out.println("Error al desencriptar");
            return null;
        }
    }

    private void showList() {
        class CrearLista extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    try {
                        elsocket = new Socket(HOST, PUERTO);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ListaMensajes = null;

                    PrintWriter pw = new PrintWriter(elsocket.getOutputStream(), true);
                    pw.println("-muestraListaMensajes19930914-" + nombreUser + "-1993End09Of14Stream-");

                    InputStreamReader lectorListaMensajes = new InputStreamReader(elsocket.getInputStream(), "UTF-8");
                    BufferedReader brListaMensajes = new BufferedReader(lectorListaMensajes);
                    String resultado = brListaMensajes.readLine();

                        byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");

                        MessageDigest sha;

                        sha = MessageDigest.getInstance("SHA-1");

                        miclaveBits = sha.digest(miclaveBits);

                        miclaveBits = Arrays.copyOf(miclaveBits, 16);

                        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

                        String listado = desencriptar(resultado, claveTrans);

                    String[] mensajesSinOrdenar = listado.split("-1993SeparadorGrande14-");

                    if(mensajesSinOrdenar.length < 1){

                        cancel(true);
                    }else {

                        ListaMensajes = new ArrayList<Mensaje>();

                        for (int i = 0; i < mensajesSinOrdenar.length; i++) {

                            try {
                                String[] singleMensaje;
                                singleMensaje = mensajesSinOrdenar[i].split("-1993Separador14-");

                                Mensaje mensaj = new Mensaje(singleMensaje[0], singleMensaje[1], singleMensaje[2], singleMensaje[3]);

                                ListaMensajes.add(mensaj);
                            } catch (Exception ie) {
                                ListaMensajes.clear();


                                ie.printStackTrace();


                                break;

                                //cancel(true);
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();

                Intent i = new Intent(mcont, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); //Limpia el arbol de activitys.

                error = "si";//Solo debe saltar cuando el error es fatal.


                mcont.startActivity(i);


            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                initializeAdapter();
            }
        }
        new CrearLista().execute();
    }

}
