package com.project.pablo.meetguild;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class UserNameChooserActivity extends AppCompatActivity {


    Button botonEnviar;
     EditText nameInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name_chooser);

        nameInput= (EditText) findViewById(R.id.editTextUserName);

        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(nameInput, InputMethodManager.SHOW_IMPLICIT);

        botonEnviar = (Button) findViewById(R.id.btn_UserName);

        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String tempString = nameInput.getText().toString();
                Boolean error = false;
                tempString =  tempString.trim();
                
                if(tempString.isEmpty()){

                    Toast.makeText(UserNameChooserActivity.this, getResources().getString(R.string.toast_error_campo_vacio), Toast.LENGTH_SHORT).show();
                    error = true;
                }
                if(tempString.length() > 10){
                    Toast.makeText(UserNameChooserActivity.this, "NO TRADUCIDO: EL NOMBRE DEBE SER MENOS O IGUAL A 10 CARACTERES.", Toast.LENGTH_SHORT).show();
                    error = true;
                }


                if(!error) {

                    new EnviaNombreUser().execute();

                }
            }
        });





    }


    class EnviaNombreUser extends AsyncTask<Void, Void, Void> {


        String username = nameInput.getText().toString();
        String serverResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                PrintWriter pw = null;
                pw = new PrintWriter(Conectador.skcli.getOutputStream(), true);
                pw.println(username);

                InputStreamReader lector = new InputStreamReader(Conectador.skcli.getInputStream(), "UTF-8");

                BufferedReader br = new BufferedReader(lector);

                serverResponse = br.readLine();




            } catch (IOException e) {
                e.printStackTrace();
                cancel(true);

            }


            return null;

        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(serverResponse.contains("user_name_exist")){


                Toast.makeText(UserNameChooserActivity.this, getResources().getString(R.string.user_name_chooser_name_exist), Toast.LENGTH_SHORT).show();

            }else if(serverResponse.contains("user_name_ok")) {


                

                if (isCancelled()) {

                    Toast.makeText(UserNameChooserActivity.this, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();
                    finish();
                } else {

                    Intent i = new Intent(getApplicationContext(), MainActivity.class);

                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                    getApplicationContext().startActivity(i);
                }

            }else{
                Toast.makeText(UserNameChooserActivity.this, R.string.toast_error_disconnect, Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }
}
