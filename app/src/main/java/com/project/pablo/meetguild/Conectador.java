package com.project.pablo.meetguild;

import org.apache.commons.codecpro.binary.Base64;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Conectador {

    //String HOST = "ec2-52-31-207-239.eu-west-1.compute.amazonaws.com";


    KeyPairGenerator keygen;
    static public KeyPair claveRSA;

    public static String miclaveAESdescifrada = null;
    public static Socket skcli = null;

    public static String respuestaServer;
    public static String userNameAndGuild = "vacio";
    public static String respuestaServerDiferente;
    public static String user;
    public static String guild;


    public Conectador(Socket elsocket) throws Exception {

        skcli = elsocket;

        PrintWriter pw = null;
        //  try {
        respuestaServer = null;

        try {
            keygen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        System.out.println("generando par de claves");

        System.out.println("Default Charset=" + Charset.defaultCharset());

        //   try {
        claveRSA = keygen.generateKeyPair();




        String cadenaId = g_sign_SignInActivity.tempAccId;

        pw = new PrintWriter(skcli.getOutputStream(), true);

        pw.println(clavePublicaToString());

        InputStreamReader lector2 = new InputStreamReader(skcli.getInputStream(), "UTF-8");

        BufferedReader br2 = new BufferedReader(lector2);

        String clavecifradaprueba;

        clavecifradaprueba = br2.readLine();

        System.out.println(clavecifradaprueba + " -> clave AES cifrada");
        miclaveAESdescifrada = descifrarclaveAEScifrada(clavecifradaprueba);
        //Llegamos al punto en el que tenemos la clave AES. Ahora podemos cifrar el usuario y contraseña y enviarlo al servidor.

        System.out.println(miclaveAESdescifrada + " -> clave AES ");

        //Avisamos al hilo recibidor que ya puede comenzar.

        // Toast.makeText(ChatMainActivity., miclaveAESdescifrada, Toast.LENGTH_SHORT).show();


        byte[] miclaveBits=miclaveAESdescifrada.getBytes("UTF-8");
        //Le aplicamos a la clave la firma HASH en SHA-1
        MessageDigest sha=MessageDigest.getInstance("SHA-1"); //Usamos SHA-1 por que siempre es multiplo de 16
        //lo cual es obligatorio en clave transparente

        miclaveBits = sha.digest(miclaveBits);
        //Cogemos los 128 bits primeros de la clave en HASH.
        miclaveBits= Arrays.copyOf(miclaveBits, 16);

        //Este implementa una interfaz de clave transparente.
        SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES");

        String mensajeencriptado = null;
        //mensajeencriptado =  encriptarAES(mensajeFirmado, claveTrans);

        mensajeencriptado =  encriptarAES(cadenaId, claveTrans);

        PrintWriter pw2 = null;
        pw2 = new PrintWriter(skcli.getOutputStream(), true);

        pw2.println(mensajeencriptado); //Se manda el id, que representa el login.


        InputStreamReader lectorRespuestaServer = new InputStreamReader(skcli.getInputStream(), "UTF-8");

        BufferedReader brRespuestaServer = new BufferedReader(lectorRespuestaServer);

         respuestaServer = brRespuestaServer.readLine();

        System.out.println("Tenemos respuesta del server: " + respuestaServer);




/*
        if(respuestaServer.equals("user_registrado")) {

            InputStreamReader lectorGetUserNameAndGuild = new InputStreamReader(elsocket.getInputStream(), "UTF-8");

            BufferedReader bruserNameAndGuild = new BufferedReader(lectorGetUserNameAndGuild);
             userNameAndGuild = bruserNameAndGuild.readLine();

            System.out.println("Esperando user y guild");
            System.out.println("Recibido user y guild, intentando lanzar ventana GUILD");

            String[] userAndGuild = Conectador.userNameAndGuild.split("-19930914-");

             user = userAndGuild[0];
             guild = userAndGuild[1];


            System.out.println(user + "---------- " + guild);



        }
*/

        if(skcli.isClosed()){

            System.out.println("Socket cerrado, cliente");
        }



    }

    private String clavePublicaToString(){

        PublicKey miclaveRSA =  claveRSA.getPublic();
        byte[] array = miclaveRSA.getEncoded();
        String clavePublica = "";


        try {
            clavePublica = new String(new Base64().encode(array), "UTF-8");



            System.out.println(clavePublica);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        return clavePublica;
    }
    public String descifrarclaveAEScifrada(String mstring) throws Exception  {

        byte[] buffer = new byte[0];
        try {
            buffer = mstring.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        }
        try {
            Cipher rsa;
            rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            rsa.init(Cipher.DECRYPT_MODE, claveRSA.getPrivate());
            byte[] utf8;
            byte[] depaso = new Base64().decode(buffer);

            utf8 = rsa.doFinal(depaso);
            return new String(utf8, "UTF-8");
        } catch (Exception e) {

            System.out.println(e.toString());
        }
        return null;
    }

    public String encriptarAES(String mensaje, SecretKey clave){

        Cipher c = null;
        byte[] criptograma = null;
        try {
            c = Cipher.getInstance("AES/ECB/PKCS5PADDING");

            c.init(Cipher.ENCRYPT_MODE, clave);
            byte[] encVal = c.doFinal(mensaje.getBytes("UTF-8"));

            //Debemos hacer un PADDING a Base64bits para que funcione correctamente.
            //Para esto, nos descargamos un archivo llamado:  commons-codec 1.10.jar
            criptograma = new Base64().encode(encVal);

        } catch (Exception e) {
            e.printStackTrace();
        }



        return new String(criptograma, Charset.defaultCharset());


    }

}
