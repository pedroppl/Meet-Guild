package com.project.pablo.meetguild;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;



public class RVAdapterListaMensajes extends RecyclerView.Adapter<RVAdapterListaMensajes.ListaMensajesViewHolder> {


    static Context mcontext;
    static String username;

    public static class ListaMensajesViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView fecha;
        TextView texto;//Probablemente esto sea la descripcion
        TextView nombre;
        TextView web;
        String estado;

        ListaMensajesViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            fecha = (TextView)itemView.findViewById(R.id.txt_fecha_mensaje);
            texto = (TextView)itemView.findViewById(R.id.txt_texto_mensaje);
            nombre = (TextView) itemView.findViewById(R.id.txt_nombreusuario_mensaje);
            web = (TextView) itemView.findViewById(R.id.txt_mensaje_web);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   // Toast.makeText(mcontext, "No implementado", Toast.LENGTH_SHORT).show();

                    /*
                    Intent i = new Intent(mcontext, EventoSingleEventActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//necesario para lanzar una activity desde una clase o metodo static

                    i.putExtra("fecha", fecha.getText().toString());
                    i.putExtra("usuario", nombre.getText().toString());
                    i.putExtra("texto", texto.getText().toString());

                    mcontext.startActivity(i);
                    */
                }
            });
        }

    }

    List<Mensaje> ListaMensajes;

    RVAdapterListaMensajes(List<Mensaje> ListaMensajes, Context ctx, String nombreuser){
        this.ListaMensajes =  ListaMensajes;

        username = nombreuser;
        mcontext = ctx;//Recuperamos el contexto y lo almacenamos.
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ListaMensajesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mensaje, viewGroup, false);
        ListaMensajesViewHolder svh = new ListaMensajesViewHolder(v);

        return svh;
    }

    @Override
    public void onBindViewHolder(ListaMensajesViewHolder mensajeViewHolder, int i) {
        mensajeViewHolder.fecha.setText(ListaMensajes.get(i).fecha);
        mensajeViewHolder.texto.setText(ListaMensajes.get(i).texto);
        mensajeViewHolder.nombre.setText(ListaMensajes.get(i).usuario);
        mensajeViewHolder.web.setText(ListaMensajes.get(i).web);


        if(!mensajeViewHolder.web.getText().toString().equals("no_web")){
            mensajeViewHolder.web.setVisibility(View.VISIBLE);
        }

        try {
            Linkify.addLinks(mensajeViewHolder.web, Linkify.WEB_URLS);
        }catch(Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {

        try {
            return ListaMensajes.size();
        }catch(Exception e){
            return 0;
        }
    }



}
