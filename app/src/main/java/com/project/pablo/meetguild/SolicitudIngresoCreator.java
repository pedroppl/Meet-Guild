package com.project.pablo.meetguild;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codecpro.binary.Base64;

import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SolicitudIngresoCreator extends AppCompatActivity {


    TextView nombreGuild;
    EditText comentario;
    Button boton_enviar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitud_ingreso_creator);


        Bundle extras = getIntent().getExtras();

        nombreGuild = (TextView) findViewById(R.id.txt_SolicitudNombreGuild);
        comentario = (EditText) findViewById(R.id.edText_comentarioSolicitud);
        boton_enviar = (Button) findViewById(R.id.btn_enviarSolicitud);
        String guildName = extras.getString("guild");

        nombreGuild.setText(guildName);



        boton_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                comentario.setText(comentario.getText().toString().trim());
                if(comentario.getText().toString().equals("")){

                    Toast.makeText(SolicitudIngresoCreator.this, R.string.toast_error_campo_vacio, Toast.LENGTH_SHORT).show();
                }else{


                    enviarSolicitudEncriptada(comentario.getText().toString());

                }



            }
        });


    }


    public void enviarSolicitudEncriptada(String comentario){

        final String mensaje;

        mensaje = nombreGuild.getText().toString() + "separador19930914Guild" + comentario;

        Thread enviador = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    byte[] miclaveBits = Conectador.miclaveAESdescifrada.getBytes("UTF-8");
                    //Le aplicamos a la clave la firma HASH en SHA-1
                    MessageDigest sha = MessageDigest.getInstance("SHA-1"); //Usamos SHA-1 por que siempre es multiplo de 16
                    //lo cual es obligatorio en clave transparente

                    miclaveBits = sha.digest(miclaveBits);
                    //Cogemos los 128 bits primeros de la clave en HASH.
                    miclaveBits = Arrays.copyOf(miclaveBits, 16);

                    //Este implementa una interfaz de clave transparente.
                    SecretKeySpec claveTrans = new SecretKeySpec(miclaveBits, "AES/ECB/PKCS5Padding");

                    String mensajeencriptado = null;


                    mensajeencriptado = encriptarAES(mensaje, claveTrans);


                    PrintWriter pw = new PrintWriter(Conectador.skcli.getOutputStream(), true);
                    pw.println("solicitud19930914-"+mensajeencriptado);


                    finish();

                    //br.close();



                } catch (Exception e) {
                    System.err.println("Error de conexion");
                    e.printStackTrace();
                }


            }
        });

        enviador.start();




    }
    public String encriptarAES(String mensaje, SecretKey clave){

        Cipher c = null;
        byte[] criptograma = null;
        try {
            c = Cipher.getInstance("AES/ECB/PKCS5Padding");

            c.init(Cipher.ENCRYPT_MODE, clave);
            byte[] encVal = c.doFinal(mensaje.getBytes("UTF-8"));

            //Debemos hacer un PADDING a Base64bits para que funcione correctamente.
            //Para esto, nos descargamos un archivo llamado:  commons-codec 1.10.jar
            criptograma = new Base64().encode(encVal);

        } catch (Exception e) {
            e.printStackTrace();
        }



        return new String(criptograma, Charset.defaultCharset());


    }


}
